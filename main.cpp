#include "Enum.hpp"
#include "Game.hpp"
#include "Interaction.hpp"

int main() {

  Interaction::clear();

  Game game;
  game.update();

  return 0;
}
