#pragma once

#include <iostream>
#include <string>

#include "Piece.hpp"

class Knight : public Piece {

private:

public:
  Knight(int id, int row, int col, Color color, PieceType type = KNIGHT, std::string name = "knight");
  ~Knight();

  static std::vector<int> getCells(int x, int y);

  const bool isConformMove(int row, int col) override;
};