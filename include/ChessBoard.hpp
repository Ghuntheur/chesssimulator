#pragma once

#include <iostream>
#include <map>
#include <vector>

#include "Enum.hpp"
#include "Error.hpp"
#include "Interaction.hpp"
#include "Piece.hpp"
#include "Pawn.hpp"
#include "Queen.hpp"
#include "King.hpp"
#include "Bishop.hpp"
#include "Rook.hpp"
#include "Knight.hpp"

class King;

class ChessBoard {

private:
  // std::vector<std::vector<std::pair<PieceType, Color>>> initState = {
  //     {{ROOK, BLACK}, {EMPTY, BLACK}, {BISHOP, BLACK}, {QUEEN, BLACK}, {KING, BLACK}, {BISHOP, BLACK}, {KNIGHT, BLACK}, {ROOK, BLACK}},
  //     {{EMPTY, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}},
  //     {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //     {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //     {{PAWN, BLACK}, {EMPTY, NO_COLOR}, {BISHOP, WHITE}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //     {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {PAWN, WHITE}, {EMPTY, NO_COLOR}, {QUEEN, WHITE}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //     {{PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {EMPTY, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}},
  //     {{ROOK, WHITE}, {KNIGHT, WHITE}, {BISHOP, WHITE}, {EMPTY, WHITE}, {KING, WHITE}, {EMPTY, WHITE}, {KNIGHT, WHITE}, {ROOK, WHITE}},
  // };
  std::vector<std::vector<std::pair<PieceType, Color>>> initState = {
    {{ROOK, BLACK}, {KNIGHT, BLACK}, {BISHOP, BLACK}, {QUEEN, BLACK}, {KING, BLACK}, {BISHOP, BLACK}, {KNIGHT, BLACK}, {ROOK, BLACK}},
    {{PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}, {PAWN, BLACK}},
    {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
    {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
    {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
    {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
    {{PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}, {PAWN, WHITE}},
    {{ROOK, WHITE}, {KNIGHT, WHITE}, {BISHOP, WHITE}, {QUEEN, WHITE}, {KING, WHITE}, {BISHOP, WHITE}, {KNIGHT, WHITE}, {ROOK, WHITE}},
  };

  // std::vector<std::vector<std::pair<PieceType, Color>>> initState = {
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {KING, BLACK}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {QUEEN, WHITE}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {KNIGHT, BLACK}, {EMPTY, NO_COLOR}},
  //   {{EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}},
  //   {{KING, WHITE}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}, {EMPTY, NO_COLOR}}
  // };

  std::map<int, Piece *> m_pieces;
  std::map<Color, int> m_kingLastPosition;


public:
  Piece* getPiece(int id) const;
  King* getKing(Color color);
  std::vector<std::pair<int, int>> getMoves(std::vector<Piece*> playerPieces);
  int getIteration(int startX, int startY, int endX, int endY);

  void setKingLastPosition(Color color, int id);

  void init();
  void constructPieces(int id, int row, int col, PieceType type, Color color);

  bool isEmptyCell(int id) const;
  bool canMove(Piece *piece);
  bool moveCollides(int id, Piece *piece);
  bool detectCollision(int start, int end, int iterate);

  Piece* detectCheckCollision(int start, int end, int iterate, Color color);

  void move(Piece * piece, int id);

  friend std::ostream& operator<<(std::ostream &os, const ChessBoard &board);
};


