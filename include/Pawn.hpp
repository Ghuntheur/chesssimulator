#pragma once

#include <iostream>
#include <string>

#include "Piece.hpp"
#include "ChessBoard.hpp"

class Pawn : public Piece {

private:
  bool m_started;
  int m_direction;

public:
  Pawn(int id, int row, int col, Color color, PieceType type = PAWN, std::string name = "pawn", bool started = false);
  ~Pawn();

  /* ----------------------------------- */

  bool started();
  int getDirection();

  /* ------------------------------------- */

  void setStarted(bool started);

  /* -------------------------------------- */

  const bool isConformMove(int row, int col) override;
};