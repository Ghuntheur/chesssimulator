#pragma once

#include <iostream>
#include <regex>
#include <string>
#include <utility>

class Interaction {

public:
  static int userChoice(bool firstTime = true);
  static bool isRow(char &row);
  static bool isCol(char &col);
  static int getTileId(int row, int col);
  static int parseRow(char &row);
  static int parseCol(char &col);
  static int getRow(int id);
  static int getCol(int id);

  static void clear();
};


