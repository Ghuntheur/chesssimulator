#pragma once

#include <vector>

#include "Enum.hpp"
#include "Player.hpp"

#include "Piece.hpp"
#include "ChessBoard.hpp"

class Game {

private:
  ChessBoard m_gameboard;
  std::vector<Player> m_players; // Admit we have two players
  int m_tour;

  static GameState m_gameState;

  // private method
  void initPlayers();

public:
  Game();
  void update();
  void play();
  const bool isEnd() const;


  // SETTER
  static void setGameState(GameState state);
};

