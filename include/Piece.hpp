#pragma once

#include <iostream>
#include <string>

#include "Enum.hpp"
#include "Interaction.hpp"
#include "Error.hpp"

class ChessBoard;

class Piece {

protected:
  int m_id;
  PieceType m_type;
  std::string m_name;
  Color m_color;
  int m_row; 
  int m_col;

public:
  Piece(int id, int row, int col, Color color, PieceType type, std::string name);
  ~Piece();
  
  const int getId() const;
  const PieceType getType() const;
  const std::string getName() const;
  const Color getColor() const;
  const int getRow() const;
  const int getCol() const;

  void setId(int id);
  void setPosition(int row, int col);

  bool isPiece();
  bool isKing();
  bool isKnight();
  bool isPawn();
  bool isQueen();
  bool isRook();
  bool isBishop();

  bool isOpponentPiece(Piece *piece);
  bool isSameColor(Color color);
  bool isPieceMove(int id);

  virtual const bool isConformMove(int row, int col) = 0;
};