#pragma once

#include <string>

#include "Enum.hpp"
#include "Interaction.hpp"
#include "Piece.hpp"
#include "ChessBoard.hpp"

class Player {

private:
  int m_id;
  std::string m_name;
  Color m_color;

public:
  Player(int id, Color color, std::string name);

  void play(ChessBoard& gameboard);
  bool isOwnPiece(Piece *piece);

  // GETTER
  const int getId() const;
  const std::string getName() const;
  const Color getColor() const;

};