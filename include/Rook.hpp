#pragma once

#include <iostream>
#include <string>

#include "Piece.hpp"

class Rook : public Piece {

private:

public:
  Rook(int id, int row, int col, Color color, PieceType type = ROOK, std::string name = "rook");
  ~Rook();

  const bool isConformMove(int row, int col) override;
};