#pragma once

#include <iostream>
#include <string>

#include "Piece.hpp"
#include "ChessBoard.hpp"

class King : public Piece {

private:
  bool m_check;
  bool m_mate;

public:
  King(int id, int row, int col, Color color, PieceType type = KING, std::string name = "king", bool check = false, bool mate = false);
  ~King();

  bool isChecked(ChessBoard *board);
  bool isCheckedHorizontalOrVertical(ChessBoard *board);
  bool isCheckedDiagonal(ChessBoard *board);
  bool isCheckedByKnight(ChessBoard *board);
  bool isCheckedByPawn(ChessBoard *board);

  bool isMat(ChessBoard *board);

  const bool isConformMove(int row, int col) override;
};