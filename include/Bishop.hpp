#pragma once

#include <iostream>
#include <string>

#include "Piece.hpp"

class Bishop : public Piece {

private:

public:
  Bishop(int id, int row, int col, Color color, PieceType type = BISHOP, std::string name = "bishop");
  ~Bishop();

  const bool isConformMove(int row, int col) override;
};