#pragma once

#include <iostream>
#include <string>

#include "Piece.hpp"

class Queen : public Piece {

private:

public:
  Queen(int id, int row, int col, Color color, PieceType type = QUEEN, std::string name = "queen");
  ~Queen();

  const bool isConformMove(int row, int col) override;
};