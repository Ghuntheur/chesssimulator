#include "Interaction.hpp"


int Interaction::userChoice(bool firstTime) {
  std::string output;
  do {
    std::cout << (firstTime ? "Choisir une pièce : " : "Déplacer à la case : ");
    std::cin >> output;
  } while (output.size() != 2 || !Interaction::isRow(output[1]) || !Interaction::isCol(output[0]));
  
  return Interaction::getTileId(Interaction::parseRow(output[1]), Interaction::parseCol(output[0]));
}

int Interaction::parseRow(char &row) {
  return 8 - atoi(&row);
}

int Interaction::parseCol(char &col) {
  return toupper(col) - 'A';
}

// can regroup this two methods in one

bool Interaction::isRow(char &row) {
  int number = atoi(&row);
  return number >= 1 && number <= 8;
}

bool Interaction::isCol(char &col) {
  int upper = toupper(col);
  return upper >= 'A' && upper <= 'H';
}

int Interaction::getTileId(int row, int col) {
  if (row < 0 || col < 0 || row > 7 || col > 7) {
    return -1;
  }
  return row * 8 + col;
}

int Interaction::getRow(int id) {
  return id/8;
}

int Interaction::getCol(int id) {
  return id%8;
}

void Interaction::clear() {
  #ifdef WIN32
    system("cls");
  #else
    system("clear");
  #endif  
}