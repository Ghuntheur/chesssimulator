#include "Piece.hpp"

Piece::Piece(int id, int row, int col, Color color, PieceType type, std::string name)
  : m_id(id), m_row(row), m_col(col), m_color(color), m_type(type), m_name(name) { }

Piece::~Piece() { }

/* ------------------------------ */

const int Piece::getId() const {
  return this->m_id;
}

const std::string Piece::getName() const {
  return this->m_name;
}

const PieceType Piece::getType() const {
  return this->m_type;
}

const Color Piece::getColor() const {
  return this->m_color;
}

const int Piece::getRow() const {
  return this->m_row;
}

const int Piece::getCol() const {
  return this->m_col;
}

/* ----------------------------- */ 

void Piece::setId(int id) {
  this->m_id = id;
}

void Piece::setPosition(int row, int col) {
  this->m_row = row;
  this->m_col = col;
}

/* ----------------------------- */

bool Piece::isPiece() {
  return this != nullptr;
}

bool Piece::isKing() {
  return this->m_type == KING;
}

bool Piece::isKnight() {
  return this->m_type == KNIGHT;
}

bool Piece::isPawn() {
  return this->m_type == PAWN;
}

bool Piece::isQueen() {
  return this->m_type == QUEEN;
}

bool Piece::isRook() {
  return this->m_type == ROOK;  
}

bool Piece::isBishop() {
  return this->m_type == BISHOP;
}

bool Piece::isOpponentPiece(Piece *piece) {
  return this->getColor() != piece->getColor();
}

bool Piece::isSameColor(Color color) {
  return this->getColor() == color;
}

bool Piece::isPieceMove(int id) {

  int row = Interaction::getRow(id);
  int col = Interaction::getCol(id);

  if (!this->isConformMove(row, col))   {
    return false;
  }

  return true;
}