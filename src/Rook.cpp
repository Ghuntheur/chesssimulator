#include "Rook.hpp"

Rook::Rook(int id, int row, int col, Color color, PieceType type, std::string name)
  : Piece(id, row, col, color, type, name) { }

Rook::~Rook() { }


const bool Rook::isConformMove(int row, int col) {

  if (this->getRow() == row || this->getCol() == col) {
    return true;
  }

  Error::print(NOT_ROOK_MOVE);
  return false;

}
