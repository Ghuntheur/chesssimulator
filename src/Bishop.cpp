#include "Bishop.hpp"

Bishop::Bishop(int id, int row, int col, Color color, PieceType type, std::string name)
  : Piece(id, row, col, color, type, name) { }

Bishop::~Bishop() { }

const bool Bishop::isConformMove(int row, int col) {
  
  int x = this->getRow();
  int y = this->getCol();

  int deltaX = std::abs(row - x); 
  int deltaY = std::abs(col - y);

  if (deltaX == deltaY) {
    return true;
  }

  Error::print(NOT_BISHOP_MOVE);
  return false;
}
