#include "Knight.hpp"

Knight::Knight(int id, int row, int col, Color color, PieceType type, std::string name)
  : Piece(id, row, col, color, type, name) { }

Knight::~Knight() { }

std::vector<int> Knight::getCells(int x, int y) {
  return {
    Interaction::getTileId(x - 2, y + 1),
    Interaction::getTileId(x - 1, y + 2),
    Interaction::getTileId(x + 2, y + 1),
    Interaction::getTileId(x + 1, y + 2),
    Interaction::getTileId(x + 1, y - 2),
    Interaction::getTileId(x + 2, y - 1),
    Interaction::getTileId(x - 2, y - 1),
    Interaction::getTileId(x - 1, y - 2)
  };
}

const bool Knight::isConformMove(int row, int col) {

  int x = this->getRow();
  int y = this->getCol();

  if ( (x+1 == row && y+2 == col) || (x+2 == row && y+1 == col) ||
       (x+1 == row && y-2 == col) || (x+2 == row && y-1 == col) ||
       (x-1 == row && y-2 == col) || (x-2 == row && y-1 == col) ||
       (x-1 == row && y+2 == col) || (x-2 == row && y+1 == col)) {

    return true;

  }

  Error::print(NOT_KNIGHT_MOVE);
  return false;

}

