#include "Error.hpp"

void Error::print(ErrorType error) {
  switch (error) {
    case YOUR_PIECE:
      std::cout << "This is one of your own piece. You can't move piece here" << std::endl;
      break;
    case NOT_YOUR_PIECE:
      std::cout << "This is not your piece" << std::endl;
      break;
    case NOT_BISHOP_MOVE:
      std::cout << "This is not a bishop move" << std::endl;
      break;
    case NOT_KING_MOVE:
      std::cout << "This is not a king move" << std::endl;
      break;
    case NOT_KNIGHT_MOVE:
      std::cout << "This is not a knight move" << std::endl;
      break;
    case NOT_PAWN_MOVE:
      std::cout << "This is not a pawn move" << std::endl;
      break;
    case NOT_QUEEN_MOVE:
      std::cout << "This is not a queen move" << std::endl;
      break;
    case NOT_ROOK_MOVE:
      std::cout << "This is not a rook move" << std::endl;
      break;
    case IS_KING:
      std::cout << "You can't eat the king !" << std::endl;
      break;
    case EMPTY_CELL:
      std::cout << "No piece here" << std::endl;
      break;
    case MOVE_COLLIDE:
      std::cout << "Piece collides with another" << std::endl;
      break;
    case OWN_KING_CHECKED:
      std::cout << "You can't move because your king is checked" << std::endl;
      break;
  }
  std::cout << std::endl;
}