#include "King.hpp"

King::King(int id, int row, int col, Color color, PieceType type, std::string name, bool check, bool mate)
  : m_check(check), m_mate(mate), Piece(id, row, col, color, type, name) { }

King::~King() { }


bool King::isChecked(ChessBoard *board) {
  if (this->isCheckedHorizontalOrVertical(board)) {
    return true;
  }

  // by queen and bishp
  if (this->isCheckedDiagonal(board)) {
    return true;
  }

  // by Knights
  if (this->isCheckedByKnight(board)) {
    return true;
  }

  // by pawn
  if (this->isCheckedByPawn(board)) {
    return true;
  }

  return false;
}

bool King::isCheckedHorizontalOrVertical(ChessBoard *board) {
  int x = this->getRow();
  int y = this->getCol();
  int start = this->getId();
  Color color = this->getColor();

  // pair<int end, int iterate>
  std::vector<std::pair<int, int>> directions = {
    {Interaction::getTileId(x, 0), -1},
    {Interaction::getTileId(x, 7), 1},
    {Interaction::getTileId(0, y), -8},
    {Interaction::getTileId(7, y), 8}
  };

  int size = directions.size();
  Piece *collidePiece;

  for (int i=0; i<size; i++) {
    collidePiece = board->detectCheckCollision(start, directions[i].first, directions[i].second, color);
    if (collidePiece->isPiece() && (collidePiece->isQueen() || collidePiece->isRook())) {
      return true;
    }
  }
  return false;
}

bool King::isCheckedDiagonal(ChessBoard *board) {
  int x = this->getRow();
  int y = this->getCol();
  int start = this->getId();
  Color color = this->getColor();

  int col = Interaction::getCol(start);
  int row = Interaction::getRow(start);

  Piece *collidePiece;

  std::vector<std::pair<int, int>> directions = {
    {Interaction::getTileId(x-std::min(row, col), y-std::min(row, col)), -9},
    {Interaction::getTileId(x+std::min(7-row, 7-col), y+std::min(7-row, 7-col)), 9},
    {Interaction::getTileId(x-std::min(row, 7-col), y+std::min(row, 7-col)), -7},
    {Interaction::getTileId(x+std::min(7-row, col), y-std::min(7-row, col)), 7}
  };

  int size = directions.size();
  for (int i=0; i<size; i++) {
    collidePiece = board->detectCheckCollision(start, directions[i].first, directions[i].second, color);
    if (collidePiece->isPiece() && (collidePiece->isBishop() || collidePiece->isQueen())) {
      return true;
    }
  }  
  return false;
}

bool King::isCheckedByKnight(ChessBoard *board) {
  int x = this->getRow();
  int y = this->getCol();
  
  Color color = this->getColor();

  Piece *collidePiece;

  std::vector<int> cells = Knight::getCells(x, y);

  int size = cells.size();

  for (int i=0; i<size; i++) {
    int id = cells[i];
    if (!board->isEmptyCell(id)) {
      collidePiece = board->getPiece(id);
      if (collidePiece->isKnight() && !collidePiece->isSameColor(color)) {
        return true;
      }
    }
  }
  return false;
}

bool King::isCheckedByPawn(ChessBoard *board) {
  int x = this->getRow();
  int y = this->getCol();
  Color color = this->getColor();

  std::vector<int> cells = {
    Interaction::getTileId(x-1, y-1),
    Interaction::getTileId(x-1, y+1),
    Interaction::getTileId(x+1, y-1),
    Interaction::getTileId(x+1, y+1)
  };

  int size = cells.size();
  Piece *collidePiece;

  for (int i=0; i<size; i++) {
    int id = cells[i];
    if (!board->isEmptyCell(id)) {
      collidePiece = board->getPiece(id);
      if (collidePiece->isPawn() && !collidePiece->isSameColor(color)) {
        Pawn *pawn = dynamic_cast<Pawn*>(collidePiece);
        if (pawn->getRow()+pawn->getDirection() == x) {
          return true;
        }
      }
    }
  }
  return false;
}

bool King::isMat(ChessBoard *board) {
  // pas le temps de tester le mat...
  return this->m_mate;
}

const bool King::isConformMove(int row, int col) {

  int x = this->getRow();
  int y = this->getCol();

  if ( (x+1 == row && y == col) || (x-1 == row && y == col) ||
       (x == row && y+1 == col) || (x == row && y-1 == col) ||
       (x+1 == row && y+1 == col) || (x+1 == row && y-1 == col) ||
       (x-1 == row && y+1 == col) || (x-1 == row && y-1 == col)) {
    return true;
  }

  Error::print(NOT_KING_MOVE);
  return false;
  
}