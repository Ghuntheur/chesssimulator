#include "Queen.hpp"

Queen::Queen(int id, int row, int col, Color color, PieceType type, std::string name)
  : Piece(id, row, col, color, type, name) { }

Queen::~Queen() { }

const bool Queen::isConformMove(int row, int col) {
  
  int x = getRow();
  int y = getCol();

  if (x == row || y == col) {
    return true;
  }

  int deltaX = std::abs(row - x);
  int deltaY = std::abs(col - y);

  if (deltaX == deltaY) {
    return true;
  }

  Error::print(NOT_QUEEN_MOVE);
  return false;
}

