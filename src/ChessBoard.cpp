#include "ChessBoard.hpp"
#include "Game.hpp"

// colorize text in console
#define BLUE "\033[34m"
#define GREEN "\033[32m"
#define RESET "\033[0m"

void ChessBoard::init() {
  for (int row=0; row<8; row++) {
    for (int col=0; col<8; col++) {
      std::pair<PieceType, Color> tile = this->initState[row][col];
      PieceType type = tile.first;
      Color color = tile.second;
      int id = Interaction::getTileId(row, col);
      this->constructPieces(id, row, col, type, color);
    }
  }
}

void ChessBoard::constructPieces(int id, int row, int col, PieceType type, Color color) {
  switch (type) {
    case KING:
      this->m_pieces.emplace(id, new King(id, row, col, color));
      this->m_kingLastPosition.emplace(color, id);
      break;
    case QUEEN:
      this->m_pieces.emplace(id, new Queen(id, row, col, color));
      break;
    case ROOK:
      this->m_pieces.emplace(id, new Rook(id, row, col, color));
      break;
    case BISHOP:
      this->m_pieces.emplace(id, new Bishop(id, row, col, color));
      break;
    case KNIGHT:
      this->m_pieces.emplace(id, new Knight(id, row, col, color));
      break;
    case PAWN:
      this->m_pieces.emplace(id, new Pawn(id, row, col, color));
      break;
    default:
      break;
  }
}


Piece* ChessBoard::getPiece(int id) const {
  return this->m_pieces.find(id)->second;
}

King* ChessBoard::getKing(Color color) {
  int id = this->m_kingLastPosition.find(color)->second;
  return dynamic_cast<King*>(this->m_pieces.find(id)->second);
}

int ChessBoard::getIteration(int startX, int startY, int endX, int endY) {
  // horizontal
  if (startX == endX && startY != endY) return 1;
  // vertical
  if (startX != endX && startY == endY) return 8;
  // diag asc
  if (startY < endY) return 7;
  // diag desc
  return 9;
}

void ChessBoard::setKingLastPosition(Color color, int id) {
  this->m_kingLastPosition[color] = id;
}

bool ChessBoard::isEmptyCell(int id) const {
  return this->m_pieces.find(id) == this->m_pieces.end();
}

bool ChessBoard::canMove(Piece *piece) {
  int id = Interaction::userChoice(false);
  std::cout << std::endl;
  // si case vide
  if (!this->isEmptyCell(id)) {
    // ça veut dire qu'il y a une pièce, donc je la récupère
    Piece *opponentPiece = this->getPiece(id);
    // Si la case de destination a une piece de notre couleur
    if (!piece->isOpponentPiece(opponentPiece)) {
      Error::print(YOUR_PIECE);
      return false;
    }

    if (opponentPiece->isKing()) {
      Error::print(IS_KING);
      return false;
    }
  }

  // c'est une case vide ou opponent, je regarde si le mouvement est conforme
  if (!piece->isPieceMove(id)) {
    return false;
  }

  // Est-ce que je rencontre une pièce qui empecherait le déplacement
  if (this->moveCollides(id, piece)) {
    Error::print(MOVE_COLLIDE);
    return false;
  }

  int savedPos = piece->getId();
  this->move(piece, id);

  Color currentPlayerColor = piece->getColor();
  Color opponentPlayerColor = (currentPlayerColor == WHITE) ? BLACK : WHITE;

  // Est-ce que mon roi n'est pas en echec ?
  King *ownKing = this->getKing(currentPlayerColor);
  if (ownKing->isChecked(this)) {
    Error::print(OWN_KING_CHECKED);
    this->move(piece, savedPos);
    return false;
  }

  King *opponentKing = this->getKing(opponentPlayerColor);
  if (opponentKing->isChecked(this)) {
    std::cout << "ROI ADVERSE EN ECHEC" << std::endl;
    if (opponentKing->isMat(this)) {
      Game::setGameState(OVER);
    }
  }

  return true;
}

bool ChessBoard::moveCollides(int id, Piece *piece) {

  // pas de test de collsion avec le cavalier
  if (piece->isKnight()) {
    return false; 
  }

  int row = Interaction::getRow(id);
  int col = Interaction::getCol(id);

  // lecture dans le sens d'un graphique
  // x = abscisses    y = ordonées
  int x = piece->getRow();
  int y = piece->getCol();
  int pieceId = piece->getId();

  // Vérification pour le pion  
  if (piece->isPawn() && y != col && this->isEmptyCell(id)) {
    return true;
  }

  int start = (pieceId < id) ? pieceId : id;
  int end = (start == pieceId) ? id : pieceId;  
  int iterate = this->getIteration(x, y, row, col);

  return this->detectCollision(start, end, iterate);
}

bool ChessBoard::detectCollision(int start, int end, int iterate) {
  for (int i=start+iterate; i<=end-iterate; i+=iterate) {
    if (!this->isEmptyCell(i)) {
      return true; 
    }
  }
  return false;
}

Piece* ChessBoard::detectCheckCollision(int start, int end, int iterate, Color color) {
  // std::cout << "start = " << start+iterate << " end = " << end << " iterate = " << iterate << std::endl;
    
  // if start is out of board, there are no piece here  
  if (start+iterate <= 0 || start+iterate >= 63) {
    return nullptr;
  }
  
  for (int i=start+iterate; (start >= end) ? i>=end : i<=end;  i+=iterate) {
    // il y a une pièce
    if (!this->isEmptyCell(i)) {
      Piece *p = this->getPiece(i);
      // si c'est ma couleur 
      if (p->isSameColor(color)) {
        return nullptr;
      }
      return p;
    }
  }
  
  return nullptr;
}

void ChessBoard::move(Piece *piece, int id) {
  this->m_pieces[id] = piece;
  this->m_pieces.erase(piece->getId());

  if (piece->isKing()) {
    this->setKingLastPosition(piece->getColor(), id);
  }

  piece->setId(id);
  piece->setPosition(Interaction::getRow(id), Interaction::getCol(id));
}

std::ostream& operator<<(std::ostream &os, const ChessBoard &board) {
  int indice = 8;
  for (int row=0; row<8; row++) {
    os << indice-- << "  ";
    for (int col=0; col<8; col++) { 
      int id = Interaction::getTileId(row, col);
      if (!board.isEmptyCell(id)) {
        Piece *p = board.m_pieces.find(id)->second;
        PieceType type = p->getType();
        Color color = p->getColor();
        os << ((color == 0) ? BLUE : GREEN) << type << RESET << " ";
      } else {
        os << "- ";
      }
    }
    os << std::endl;
  }

  os << "\n   ";
  for (char letter='A'; letter<='H'; letter++) {
    os << letter << " ";
  }
  os << std::endl;

  return os;
}
