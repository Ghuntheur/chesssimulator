#include "Pawn.hpp"

Pawn::Pawn(int id, int row, int col, Color color, PieceType type, std::string name, bool started)
  : m_started(started), Piece(id, row, col, color, type, name) { 
    this->m_direction = (color == WHITE) ? -1 : 1;
}

Pawn::~Pawn() { }

/* ------------------------------------------------ */

bool Pawn::started() {
  return this->m_started;
}

int Pawn::getDirection() {
  return this->m_direction;
}

/* ------------------------------------------------ */


void Pawn::setStarted(bool started) {
  this->m_started = started;
}

/* ----------------------------------------------- */ 

// No eat verification yet
const bool Pawn::isConformMove(int row, int col) {
  // si pas sur la même colonne, mauvais mouvement
  int x = this->getRow();
  int y = this->getCol();
  int dir = this->getDirection();

  if ((y+1 == col || y-1 == col) && x+dir == row) {
    return true;
  }

  if (y != col) {
    Error::print(NOT_PAWN_MOVE);
    return false;
  }

  if ((!this->started() && x+2*dir == row) ||
      x+dir == row) {
    this->setStarted(true);
    return true;
  }

  Error::print(NOT_PAWN_MOVE);
  return false;
}
