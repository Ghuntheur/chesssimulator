#include "Player.hpp"

Player::Player(int id, Color color, std::string name)
  : m_id(id), m_color(color), m_name(name) { }

void Player::play(ChessBoard& gameboard) {

  std::cout << gameboard << std::endl;

  std::cout << this->getName() << ", à vous de jouer \n" << std::endl;

  Piece *piece;
  bool own;   
  do {
    int choice;
    bool notFound;
    
    do {
      choice = Interaction::userChoice();
      notFound = gameboard.isEmptyCell(choice);
      if (notFound) Error::print(EMPTY_CELL);
    } while (notFound);

    piece = gameboard.getPiece(choice);
    own = this->isOwnPiece(piece);
    if (!own) Error::print(NOT_YOUR_PIECE);
  } while(!own);

  std::cout << this->getName() << " a pris la pièce : " << piece->getName() << std::endl;

  // Le joueur rejoue si le déplacement est impossible
  if (!gameboard.canMove(piece)) {
    this->play(gameboard);
  }
}

bool Player::isOwnPiece(Piece *piece) {
  return this->getColor() == piece->getColor();
}

// GETTER

const int Player::getId() const {
  return this->m_id;
}

const std::string Player::getName() const {
  return this->m_name;
}

const Color Player::getColor() const {
  return this->m_color;
}

