#include "Game.hpp"

// init static member
GameState Game::m_gameState = BUILDING;

Game::Game() { 
  this->m_tour = 0;
}

/* --------------------------------------------- */

void Game::update() {
  switch (Game::m_gameState) {
    case BUILDING:
      this->m_gameboard.init(); // init chessboard
      this->initPlayers(); // init players
      Game::setGameState(PLAYING); // change gamestate to play
      this->update();
      break;
    case PLAYING:
      this->play();
      this->update();
      break;
    case OVER:
      std::cout << this->m_gameboard << std::endl;
      std::cout << "FIN DE LA PARTIE" << std::endl;
      break;
    default:
      break;
  }
}

void Game::initPlayers() {
  // white player
  std::string name;
  std::cout << "Saisir nom pour joueur blanc : ";
  std::getline(std::cin, name);
  this->m_players.emplace_back(Player(0, WHITE, name));
  // black player
  std::cout << "Saisir nom pour le joueur noir : ";
  std::getline(std::cin, name);
  this->m_players.emplace_back(Player(1, BLACK, name));
}

void Game::play() {
  do {
    this->m_players[this->m_tour%2].play(this->m_gameboard);
    this->m_tour++;
   } while (!this->isEnd());
} 

const bool Game::isEnd() const {
  return Game::m_gameState == OVER;
}

/* ------------------------------------------------ */

void Game::setGameState(const GameState state) {
  Game::m_gameState = state;
}


